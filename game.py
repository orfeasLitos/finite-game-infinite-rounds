from random import randint
from math import floor, log10

d = 1
c = 2*d

for games in [10, 100, 1000, 10000, 100000, 1000000]:
  total = 0
  for i in range(games):
    round = 0
    balls = c+d
    selected = 0

    while selected not in range(1, d+1):
      round += 1
      selected = randint(1, balls)
      balls += c

    #print(str(i) + "/" + str(games) + ":", round)
    total += round

  avg = total/games
  print("Games:", str(games) + ", Average rounds:", str(avg) + ", Average digits of rounds:", floor(log10(avg))+1)
